$(document).ready ->
    center = null
    marker = null
    zoom = null
    map = null

    getCenter = ->
        if $('#lng').val() == '' or $('#lat').val() == ''
            center = new (google.maps.LatLng)(48.4, 3.05)
        else
            center = new (google.maps.LatLng)($('#lat').val(), $('#lng').val())
        center

    setZoom = ->
        if $('#zoom').val() == '' or $('#zoom').val() == undefined
            map.setZoom 5
        else
            map.setZoom parseInt($('#zoom').val())
        return

    initialize = ->
        mapOptions =
            center:
                lat: -34.397
                lng: 150.644
            zoom: 8
        map = new (google.maps.Map)(document.getElementById('map-canvas'), mapOptions)
        center = getCenter()
        marker = new (google.maps.Marker)(
            position: center
            map: map
            draggable: true)
        setZoom()
        google.maps.event.addListener marker, 'dragend', (evt) ->
            $('#lat').val evt.latLng.lat()
            $('#lng').val evt.latLng.lng()
            return
        centerMap map

        google.maps.event.addListener map, 'click', (evt) ->
            marker.setPosition evt.latLng
            $('#lat').val evt.latLng.lat()
            $('#lng').val evt.latLng.lng()
            return

        return

    updateMarker = ->
        center = getCenter()
        marker.setPosition center
        map.panTo center
        return

    updateZoom = ->
        setZoom()
        return

    centerMap = ->
        center = getCenter()
        map.panTo center
        return

    $('#zoom').change ->
        updateZoom()
        return
    $('#lat').change ->
        updateMarker()
        return
    $('#lng').change ->
        updateMarker()
        return
    google.maps.event.addDomListener window, 'load', initialize

    $('#selectCity').change ->
        cityId = $(this).val()
        $.get(rootpath + '/api/cities').success (data) ->
            i = 0
            while i < data.cities.length
                if parseInt(data.cities[i].id) == parseInt(cityId)
                    $('#lat').val data.cities[i].latitude
                    $('#lng').val data.cities[i].longitude
                    updateMarker()
                    zoom=data.cities[i].zoom
                    map.setZoom parseInt(zoom)
                    break
                i++
            return
    return