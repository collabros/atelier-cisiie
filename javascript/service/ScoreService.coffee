app = angular.module 'app'

app.service 'ScoreService', ->
  @score = 0
  @history = []
  @maxDistance = 0

  #Ajout d'un score à l'historique en stockant les coordonnées
  @.add = (distance, nb, time,location,originLocation)->
      score = @.calcul distance, time
      @history[nb] = {score : score,distance :distance,time : time,location : {lat : location.lat(),lng : location.lng()}, originLocation : {lat :originLocation.lat(),lng:originLocation.lng()}}
      @score = @score + score

  # Calcul du score selon la distance et le temps
  @.calcul = (distance, time)->
      score = 0
      # si distance > 2D
      if distance > (2 * @maxDistance)
        score = 1
      else
        # Si D < distance < 2D
        if distance < (2 * @maxDistance) && distance > @maxDistance
          score = 3
        else
          # si distance < D
          if distance < @maxDistance
            score = 5
      # si t < 2
      if time < 2
        score *= 4
      else
        # si 2 < t < 5
        if time > 2 && time < 5
          score *= 2
        # si t > 10
        else if time > 10
          score = 0
      return score

  @.setMaxDistance = (distance)->
      @maxDistance = distance * 100

  @.reset = ->
    @score = 0
    @history = []

  @.load = (history) ->
    @history = history
    for value in history
      @score+= value.score

  @