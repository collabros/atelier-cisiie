<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Game extends Model{

    public $table = 'Game';
    public $idTable = 'id';
    public $timestamps = false;


    public function photos() {
        return $this->belongsToMany('model\Photo','Game_Photo','id_game','id_photo');
    }

    public function difficulty(){
        return $this->hasOne('model\Difficulty','id', 'id_difficulty');
    }

    public function city(){
        return $this->hasOne('model\City','id','id_city');
    }

    public function user(){
        return $this->belongsTo('model\User','id_user');
    }
}