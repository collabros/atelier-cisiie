<?php

namespace controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\City;
use model\User;
use model\Photo;
use \exception\AuthException;
use view\CityEditView;
use view\PhotoEditView;
use view\CityView;
use view\PhotoView;

class PhotoController extends BaseController {

    public function index() {
        if (!isset($_SESSION['id'])) {
            $this->app->flash('info', 'Veuillez vous connecter');
            $this->app->redirect($this->app->urlFor('login'));
        }
        $photos = null;
        if(array_key_exists('admin', $_SESSION)) {
            $photos = Photo::all();
        } else {
            $photos = Photo::where('id_user','=',$_SESSION['id'])->get();
        }
        foreach ($photos as $photo) {
            $photo['urlEdit'] = $this->app->urlFor('photoEdit', array('id' => $photo->id));
            $photo['city'] = $photo->city;
        }
        $view = new PhotoView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('photos', $photos);
        $view->addVar('title', 'PhotoLocate - Vos photos');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function edit($id) {
        $photo = Photo::find($id);
        if($photo == null) {
            $this->app->flash('error', 'Unknow photo');
            $this->app->redirect($this->app->urlFor('photo'));
        }
        if($_SESSION['id']==$photo->id_user || array_key_exists('admin', $_SESSION)) {
            if(array_key_exists('admin', $_SESSION)) {
                $cities = City::all();
            } else {
                $cities = City::whereNull('custom_map')->get();
            }
            $view = new PhotoEditView();
            $view->addVar('link',links());
            $view->addVar('session', $_SESSION);
            $view->addVar('photo', $photo);
            $view->addVar('city', $photo->city);
            $view->addVar('cities',$cities);
            $view->addVar('formPost', $this->app->urlFor('photoEditPost', array('id' => $photo->id)));
            $token = generateToken();
            $view->addVar('token', $token);
            $_SESSION['token'] = $token;
            $view->addVar('title', 'PhotoLocate - Administration');
            $env = $this->app->environment();
            $view->addVar('path',$env['SCRIPT_NAME']);
            echo $view->render();
        }else{
            $this->app->flash('error','Cette photo ne vous appartient pas');
            $this->app->redirect($this->app->urlFor('photo'));
        }

    }

    public function editPost($id) {
        $post = $this->app->request->post();

        if($_SESSION['token'] != $post['token']) {
            $this->app->flash('error', 'Bad token');
            $this->app->redirect($this->app->urlFor('photoEdit', array('id' => $id)));
        }


        $photo = Photo::find($id);
        if($photo == null) {
            $this->app->flash('error', 'Unknown city');
            $this->app->redirect($this->app->urlFor('photo'));
        }

        $city = City::find($post['cities']);
        if($city == null || ($city->custom_map != null && !array_key_exists('admin', $_SESSION))) {
            $this->app->flash('error', 'Unknown city');
            $this->app->redirect($this->app->urlFor('photo'));
        }

        if($_SESSION['id']==$photo->id_user) {

            if (array_key_exists("delete", $post)) {
                $url = $photo->url;
                $photo->games()->detach();
                $photo->delete();
                unlink(ROOT_PATH . $url);
                $this->app->flash('success', 'Photo supprimée avec succès');
                $this->app->redirect($this->app->urlFor('photo'));

            } else {
                $data_array = $post;
                $data_array['city'] = $city;
                $data_array['id_user'] = $_SESSION['id'];
                $data_array['id_photo'] = $photo->id;
                $data_array['edit'] = true;
                $photo = self::setterPhoto($data_array);
                if ($_FILES['photo']['error'] == 0)
                    $photo = self::sanitizePhotos($_FILES, $photo, $data_array['city']->name);
                $photo->save();
                $this->app->flash('success', 'Photo modifiée avec succès');
                $this->app->redirect($this->app->urlFor('photo'));
            }
        } else {
            $this->app->flash('error','Cette photo ne vous appartient pas');
            $this->app->redirect($this->app->urlFor('photo'));
        }
    }

    public function add() {
        $view = new PhotoEditView();
        if(array_key_exists('admin', $_SESSION)) {
            $cities = City::all();
        } else {
            $cities = City::whereNull('custom_map')->get();
        }
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('cities', $cities);
        $view->addVar('formPost', $this->app->urlFor('photoAddPost'));
        $token = generateToken();
        $view->addVar('token', $token);
        $_SESSION['token'] = $token;
        $view->addVar('title', 'PhotoLocate - Ajout d\'une photo');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function addPost() {
        $post = $this->app->request->post();

        if($_SESSION['token'] != $post['token']) {
            $this->app->flash('error', 'Bad token');
            $this->app->redirect($this->app->urlFor('photoAdd'));
        }

        $city = City::find($post['cities']);
        if($city == null || ($city->custom_map != null && !array_key_exists('admin', $_SESSION))) {
            $this->app->flash('error', 'Unknown city');
            $this->app->redirect($this->app->urlFor('photo'));
        }

        //TODO
        //Gestion d'upload
        $data_array = $post;
        $data_array['city'] = $city;
        $data_array['id_user'] = $_SESSION['id'];
        $photo = self::setterPhoto($data_array);
        self::sanitizePhotos($_FILES['photo'],$photo,$data_array['city']->name)->save();
        $this->app->flash('success', 'Photo ajoutée avec succès');
        $this->app->redirect($this->app->urlFor('photo'));

    }

    public static function sanitizePhotos($file,$photo ,$cityname) {

        $city_name = preg_replace("#[^A-Za-z0-9]#","_",$cityname);
        $filename = preg_replace("#[^A-Za-z0-9]#","_",$file['name']);
        $uploaddir = "photos/$city_name/";

        if ($file['error'] == 0 ) {

            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0755, true);
            }
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            $uploadfile = $uploaddir . basename($file['tmp_name']) . '_' . $filename . "." . $extension;

            if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
                $photo->url = $uploadfile;
                return $photo;
            }
            else {
                $errors[]="Problème d'upload de ".$file['name'];
            }
        }
        else {
            if ($file['error'] != 4) {
                $errors[]="Problème d'upload de ".$file['name'];
            }
        }
    }

    public static function setterPhoto($data_array){
        if(isset($data_array['id_photo']))
            $photo = Photo::find($data_array['id_photo']);
        else
            $photo = new Photo();
        $photo->latitude = $data_array['latitude'];
        $photo->longitude = $data_array['longitude'];
        $photo->id_city = $data_array['city']->id;
        $photo->id_user = $data_array['id_user'];
        if ($data_array['edit'] = true)
            $photo->created_at = date('Y-m-d H:i:s');
        return $photo;
    }



}