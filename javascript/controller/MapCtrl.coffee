app = angular.module 'app'

#Affichage de la map et logique métier
app.controller 'MapCtrl', ['$scope', '$http','ScoreService' ,'$q', 'checkToken', '$rootScope', '$routeParams', '$location', ($scope, $http, ScoreService, $q, checkToken, $rootScope, $routeParams, $location) ->



    #Creation de la variable map, qui contiendra la map google
    map = null



    if localStorage.id_game == undefined
        modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes navrés mais nous n'avons pas réussi à charger la partie.", button1:"Redirection",pressed1:'/choose-city'}
        $rootScope.$broadcast('modal', modal)
    else
        id_game = JSON.parse localStorage.id_game

    if localStorage.token == undefined
        modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes navrés mais nous n'avons pas réussi à vous identifier.", button1:"Redirection",pressed1:'/choose-city'}
        $rootScope.$broadcast('modal', modal)
    else
        token = JSON.parse localStorage.token
        checkToken('/')

    if localStorage.id_user == undefined
        modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes navrés mais nous n'avons pas réussi à vous identifier.", button1:"Redirection",pressed1:'/choose-city'}
        $rootScope.$broadcast('modal', modal)

    if $scope.errors == undefined
        urlGame = rootpath+"/play/games/" + id_game #Si succès, on renvoie les données avec resolve au deferer, sinon, on les rejette au deferer
        $http.get(urlGame)
            .success (data) ->
                assignGame(data)
            .error (data) ->
                erreur(data)

    originLocation = null
    if localStorage.progress != undefined
        prog = Object.keys(JSON.parse localStorage.progress).length
    if prog == undefined
        value = 0
    else
        value = prog
    line = null
    markerClient = null
    markerOrigin = null
    duration = 10000
    mapCenterLocation = null
    photos = null
    city = null
    lotrMapType = null;
    gotMapType = null;

    assignGame = (dataGame) ->
        game = dataGame.results
        if game.finished_at != null
            modal = {scope:$scope, status:true,title:"Erreur", message: "Partie non détéctée", button1:"Redirection",pressed1:'/choose-city'}
            $rootScope.$broadcast('modal', modal)

        city = game.city

        if city.custom_map == "lotr"
            lotrMapType = new google.maps.ImageMapType(lotrOptions);
        else if city.custom_map == "got"
            gotMapType = new google.maps.ImageMapType(gotOptions);

        photos = game.photos
        $scope.photoTotal = photos.length

        if prog == undefined
            $scope.photoProgress = 1
            $scope.scoreService = ScoreService
            $scope.scoreService.reset()
            $scope.scoreService.setMaxDistance game.difficulty.distance
        else
            scoreServiceCourant = JSON.parse localStorage.scoreService
            historyCourant = scoreServiceCourant.history
            scoreCourant = scoreServiceCourant.score
            if game.difficulty.nb_photo == prog
                urlGame = rootpath+"/play/games/" + id_game
                $http.put(urlGame, {score: scoreCourant})
                .success (data) ->
                    $location.path '/results'
                .error () ->
                    modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés mais nous avons rencontré une erreur.", button1:"Redirection",pressed1:'/choose-city'}
                    $rootScope.$broadcast('modal', modal)
            else
                modal = {scope:$scope, status:true,title:"Erreur", message: "La page a été fermée ou rechargée en pleine partie", button1:"Nouvelle partie",pressed1:'/', button2:"Continuer"}
                $rootScope.$broadcast('modal', modal)
                $scope.photoProgress = prog+1
                $scope.scoreService = ScoreService
                $scope.scoreService.setMaxDistance game.difficulty.distance
                $scope.scoreService.history = historyCourant
                $scope.scoreService.score = scoreCourant

        if photos.status? == 404
            modal = {scope:$scope, status:true,title:"Erreur", message: photos.message, button1:"Redirection",pressed1:'/choose-city'}
            $rootScope.$broadcast('modal', modal)

        #Définition de 2 points coordonnées : la première pour centrer la map
        mapCenterLocation = new google.maps.LatLng(city.latitude, city.longitude)

        #Appel à la fonction d'initialisation quand la page est chargée
        google.maps.event.addDomListener window, 'load', initialize()


    erreur = (data) ->
        modal = {scope:$scope, status:true,title:"Erreur", message: data.message, button1:"Redirection",pressed1:'/choose-city'}
        $rootScope.$broadcast('modal', modal)


    #Chargement de la prochaine image quand le bouton Suivant est enfoncé
    $scope.nextPhoto = ->
        $scope.photoProgress = value+1;

        #On enlève les markers et la ligne et on charge la nouvelle photo
        if markerClient != null
            markerClient.setMap(null)
        if markerOrigin != null
            markerOrigin.setMap(null)
        if line != null
            line.setMap(null)


        if value < photos.length
            imageLoading = new Image()
            imageLoading.onload = () ->
                $scope.photo = imageLoading.src
                #On itère sur les photos
                value++

                #On enregistre la date actuelle
                $scope.startTime = new Date();

                #On démarre le compte à rebours
                tick()

            imageLoading.onerror = () ->
                modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes navrés, mais l'image n'est pas possible à charger.", button1:"Redirection",pressed1:'/choose-game'}
                $rootScope.$broadcast('modal', modal)

            imageLoading.src = rootpath+'/'+photos[value].url;


        #On désactive le bandeau
        $scope.bandeau = false

        #On crée un objet avec la position d'origine de la nouvelle photo
        if value < photos.length
            originLocation = new google.maps.LatLng(photos[value].latitude, photos[value].longitude)


        #On active le click listener sur la map
        google.maps.event.addListener map, 'click', (event) ->
            clickedOnMap(event)


    #Fonction de calcul de temps
    tick = () ->
        $scope.millis = new Date() - $scope.startTime

        #Prévient l'erreur de $deploy
        if (!$scope.$$phase)
            $scope.$apply ->
                $scope.progress = (duration - $scope.millis) / 100

        #Si le bandeau n'est pas affiché, on lance un timeout
        if $scope.bandeau == false
            setTimeout ->
                tick()
            , 20
            #Sinon on arrête le timer et on calcule le temps mis
        else
            if (!$scope.$$phase)
                time = (100 - $scope.progress) / 10
                timeInSeconds = Math.round(time * 100) / 100
                $scope.$apply ->
                    $scope.time = "Vous avez cliqué en " + timeInSeconds + " secondes"
                    #On calcul le score en mettant la distance , l'id de la photo ,le temps, et on stock les coordonnées de la photo et du click
                    $scope.scoreService.add $scope.distance,value-1,timeInSeconds,$scope.l1,$scope.l2
                    #Affichage des données
                    $scope.score = "Vous avez cliqué à "+$scope.scoreService.history[value-1].distance+"m"
                    #$scope.scoreService.history[value-1].score
                    $scope.time = "Vous avez cliqué en "+timeInSeconds+" secondes"

                    scoreService = {}
                    localStorage.setItem 'scoreService', JSON.stringify $scope.scoreService


    lotrOptions = {
        getTileUrl: (coord) ->
            normalizedCoord = getNormalizedCoord(coord, 10)
            if (!normalizedCoord)
                return null

            return rootpath+'/image/custom/lotr/' + normalizedCoord.y + '/' +
                    (normalizedCoord.x) + '.jpeg'
        ,
        tileSize: new google.maps.Size(240, 242),
        maxZoom: 4,
        minZoom: 4,
        name: 'Lotr'
    }

    gotOptions = {
        getTileUrl: (coord) ->
            normalizedCoord = getNormalizedCoord(coord, 5, 11)
            if (!normalizedCoord)
                return null

            return rootpath+'/image/custom/got/' + normalizedCoord.y + '/' +
                    normalizedCoord.x + '.jpeg'
        ,
        tileSize: new google.maps.Size(265, 274),
        maxZoom: 3,
        minZoom: 3,
        name: 'Got'
    }

    getNormalizedCoord = (coord, tileRX, tileRY) ->
        y = coord.y
        x = coord.x

        tileRangeX = tileRX
        tileRangeY = tileRY

        if (y < 0 || y >= tileRangeY)
            return null


        if (x < 0 || x >= tileRangeX)
            return null

        return {
            x: x,
            y: y
        }

    #Fonction d'initialisation de la map
    initialize = () ->
        if city.custom_map == 'lotr'
            myLatlng = new google.maps.LatLng(68.65655498475735,-67.5);
            mapOptions = {
                center: myLatlng,
                zoom: 4,
                streetViewControl: false,
                mapTypeControlOptions: {
                    mapTypeIds: ['lotr']
                }
            };

            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
#            markerTest = new google.maps.Marker {
#                position: myLatlng, map: map, draggable:true, icon: rootpath + "/image/regroup.png", title: "Origine"
#            }
#
#            google.maps.event.addListener markerTest, 'dragend', (evt) ->
#                console.log(evt.latLng.lat())
#                console.log(evt.latLng.lng())

            map.mapTypes.set('lotr', lotrMapType);
            map.setMapTypeId('lotr');
        else if city.custom_map == 'got'
            myLatlng = new google.maps.LatLng(-22.91792293614603,-65.0390625);
            mapOptions = {
                center: myLatlng,
                zoom: 3,
                streetViewControl: false,
                mapTypeControlOptions: {
                    mapTypeIds: ['got']
                }
            };

            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

            map.mapTypes.set('got', gotMapType);
            map.setMapTypeId('got');

            #MARKER TEST
#            markerTest = new google.maps.Marker {
#                position: myLatlng, map: map, draggable:true, icon: rootpath + "/image/regroup.png", title: "Origine"
#            }
#
#            google.maps.event.addListener markerTest, 'dragend', (evt) ->
#                console.log(evt.latLng.lat())
#                console.log(evt.latLng.lng())
        else

            #On modifie le style de la map pour afficher ce que l'on souhaite
            myStyles = [{
                featureType: "administrative", elementType: "labels", stylers: [{visibility: "off"}]
            }, {
                featureType: "poi", elementType: "labels", stylers: [{visibility: "off"}]
            }, {
                featureType: "landscape", elementType: "labels", stylers: [{visibility: "off"}]
            }, {
                featureType: "transit", elementType: "labels", stylers: [{visibility: "off"}]
            }]

            #Les options de la map
            mapOptions = {
                zoom: parseInt(city.zoom), zoomControl: false, panControl:false, streetViewControl: false, mapTypeControl: false, center: mapCenterLocation, draggableCursor: 'crosshair', styles: myStyles
            }

            #On associe la map à la div#map-canvas
            map = new google.maps.Map document.getElementById('map-canvas'), mapOptions
        google.maps.event.addListenerOnce map, 'tilesloaded', ->
            #On charge une photo
            $scope.nextPhoto()

    #Quand on a cliqué sur la map
    clickedOnMap = (event) ->

        google.maps.event.clearListeners map, 'click'

        $scope.$apply ->
            $scope.bandeau = true
            #Si on a fait toutes les photos, on affichera les résultats quand la personne aura cliqué sur la carte
            if value >= photos.length
                $scope.showResults = true

        location = event.latLng; #Suppression du listener quand on clique sur la map pour que la personne ne puisse plus ajouter de marker

        if localStorage.progress == undefined
            progress = {}
            progress[value] = location
            localStorage.setItem 'progress', JSON.stringify progress
        else
            progress = JSON.parse localStorage.progress
            progress[value] = location
            localStorage.setItem 'progress', JSON.stringify progress

        #Ajout du marker du client
        markerClient = new google.maps.Marker {
            position: location, map: map, title: "Hello World!"
        }

        #Ajout du marker d'origine de la photo
        markerOrigin = new google.maps.Marker {
            position: originLocation, map: map, icon: rootpath + "/image/regroup.png", title: "Origine"
        }


        #Ajout d'une animation
        markerOrigin.setAnimation(google.maps.Animation.DROP)

        #Ajout d'une ligne partant de la location du client vers le point d'origine
        lineCoordinates = [location, originLocation]

        #Propriétés de la ligne
        line = new google.maps.Polyline {
            path: lineCoordinates, geodesic: true, strokeColor: '#000000', strokeOpacity: 1.0, strokeWeight: 2
        }

        #Ajout de la ligne à la map
        line.setMap(map)
        #Calcul de la distance entre les deux points
        $scope.distance = Math.round(google.maps.geometry.spherical.computeDistanceBetween(location,originLocation))
        #On stock les deux positions dans le scope
        $scope.l1 = location
        $scope.l2 = originLocation



    #Quand on souhaite afficher les résultats
    $scope.results = ->
        urlGame = rootpath+"/play/games/" + id_game
        $http.put(urlGame, {score: $scope.scoreService.score})
        .success (data) ->
            $location.path '/results'
        .error () ->
            modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés mais nous avons rencontré une erreur.", button1:"Redirection",pressed1:'/choose-city'}
            $rootScope.$broadcast('modal', modal)

]