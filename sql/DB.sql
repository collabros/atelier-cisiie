CREATE DATABASE  IF NOT EXISTS `photolocate` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `photolocate`;
-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: photolocate
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `City`
--

DROP TABLE IF EXISTS `City`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `City` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zoom` int(11) DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `custom_map` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `City`
--

LOCK TABLES `City` WRITE;
/*!40000 ALTER TABLE `City` DISABLE KEYS */;
INSERT INTO `City` VALUES (11,'Nancy','6.1844509124755405','http://media-cdn.tripadvisor.com/media/photo-s/02/3c/6a/57/filename-nancy-one-com.jpg',14,'48.6918912681834','2015-02-13 17:30:00',NULL,NULL),(12,'Paris','2.3488000','http://www.impots-locaux.net/wp-content/uploads/2014/04/paris-aide-de-l-etat.jpg',12,'48.8534100','2015-02-13 17:30:00',NULL,NULL),(13,'Terre du Milieu','1','http://2.bp.blogspot.com/-JaD4Hy3_psg/UcM26i_0IfI/AAAAAAAAAg0/QH2THvfeo40/s1600/Seigneur+des+anneaux.jpg',4,'1','2015-02-12 15:22:11',NULL,'lotr'),(14,'Westeros','1','http://static.comicvine.com/uploads/original/13/130470/3049075-3830025064-Fanta.jpg',4,'1','2015-02-12 15:23:36',NULL,'got'),(15,'New York','-73.99269661631774','http://puu.sh/fT0AN/af703779b4.jpg',14,'40.729359470196584','2015-02-14 02:11:26',NULL,NULL);
/*!40000 ALTER TABLE `City` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Difficulty`
--

DROP TABLE IF EXISTS `Difficulty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Difficulty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `nb_photo` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Difficulty`
--

LOCK TABLES `Difficulty` WRITE;
/*!40000 ALTER TABLE `Difficulty` DISABLE KEYS */;
INSERT INTO `Difficulty` VALUES (5,'Facile',5,10,'2015-02-13 17:30:32',NULL),(6,'Moyen',2,15,'2015-02-13 17:30:51',NULL),(7,'Difficile',1,20,'2015-02-13 17:31:03',NULL),(8,'Marathon',3,100,'2015-02-13 17:31:16',NULL),(9,'Test',1,5,'2015-02-13 17:42:28',NULL);
/*!40000 ALTER TABLE `Difficulty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Game`
--

DROP TABLE IF EXISTS `Game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_city` int(11) DEFAULT NULL,
  `id_difficulty` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Game` (`id_city`),
  KEY `fk_Game_1` (`id_difficulty`),
  KEY `fk_Game_2` (`id_user`),
  CONSTRAINT `fk_Game` FOREIGN KEY (`id_city`) REFERENCES `City` (`id`),
  CONSTRAINT `fk_Game_1` FOREIGN KEY (`id_difficulty`) REFERENCES `Difficulty` (`id`),
  CONSTRAINT `fk_Game_2` FOREIGN KEY (`id_user`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Game`
--

LOCK TABLES `Game` WRITE;
/*!40000 ALTER TABLE `Game` DISABLE KEYS */;
INSERT INTO `Game` VALUES (125,11,9,86,NULL,'c59ff7e582d4b4ea1bf579cccff91256810ebfb2','2015-02-13 17:42:34',NULL),(126,11,9,87,NULL,'247fee8d988a0813c9a0e94a36981a8ae8eaca1a','2015-02-13 17:44:07',NULL),(127,11,9,88,23,'fdd42671be8aecf167237d97da63be19ad6ad9a2','2015-02-13 17:47:23','2015-02-13 17:50:52'),(128,13,9,90,7,'572c9928db0bb9d845b4e0729c46c9afef8ebf77','2015-02-13 23:28:59','2015-02-13 23:33:02'),(129,13,9,91,NULL,'851faa036e3626329f0e6d2ed07253a37478c114','2015-02-13 23:35:48',NULL),(130,13,9,92,NULL,'1a5e20e48e77049289ea36530e4e4e0667b74158','2015-02-14 00:01:07',NULL),(131,13,6,93,36,'6a0c50d07f821a29f2d85860f2ebc5823c03de5b','2015-02-14 00:03:41','2015-02-14 00:04:57'),(132,14,9,94,NULL,'565ce9800fcbd701120f1a2637122869f43982e0','2015-02-14 00:15:23',NULL),(133,14,9,95,9,'7a579506b7dab507dbad2a598232329606f17106','2015-02-14 01:16:51','2015-02-14 01:18:39'),(134,14,9,96,8,'6a0e8f6a48470b9bc5c832ce542603c34389c00f','2015-02-14 01:19:08','2015-02-14 01:19:35'),(135,11,9,98,20,'cfdaf82db63a25d23fb6e8b785de0e93ea06f8ef','2015-02-14 01:58:29','2015-02-14 01:58:39'),(136,11,9,99,NULL,'b179b941b5d7fd95907edbdea1d4c65d9c055302','2015-02-14 01:58:44',NULL),(137,15,9,100,NULL,'7a74897eca289d9ed7114b254488277870d08373','2015-02-14 02:20:58',NULL),(138,15,9,101,30,'9ddd92bf609a1b4173e1fe997a4fc515d2280306','2015-02-14 02:21:39','2015-02-14 02:22:04'),(139,13,5,102,NULL,'9bb2c3e251f588d3298510a9dd04c317e29b4a3d','2015-02-14 02:23:24',NULL),(140,13,9,103,NULL,'fd7ff690b273c6f1911f7cc2ab2d8cebe9d9b933','2015-02-14 02:25:03',NULL),(141,13,9,104,9,'cefecb55b3dfa1bb9a3699c6923821aaf220c3d1','2015-02-14 02:27:32','2015-02-14 02:28:15');
/*!40000 ALTER TABLE `Game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Game_Photo`
--

DROP TABLE IF EXISTS `Game_Photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Game_Photo` (
  `id_game` int(11) NOT NULL,
  `id_photo` int(11) NOT NULL,
  PRIMARY KEY (`id_game`,`id_photo`),
  KEY `fk_Game_Photo_1` (`id_photo`),
  CONSTRAINT `fk_Game_Photo` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id`),
  CONSTRAINT `fk_Game_Photo_1` FOREIGN KEY (`id_photo`) REFERENCES `Photo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Game_Photo`
--

LOCK TABLES `Game_Photo` WRITE;
/*!40000 ALTER TABLE `Game_Photo` DISABLE KEYS */;
INSERT INTO `Game_Photo` VALUES (125,17),(126,17),(127,17),(135,17),(136,17),(125,18),(126,18),(127,18),(135,18),(136,18),(125,19),(126,19),(127,19),(135,19),(136,19),(136,20),(125,21),(126,21),(127,21),(135,21),(125,22),(126,22),(127,22),(135,22),(136,22),(128,23),(129,23),(131,23),(140,23),(141,23),(128,24),(129,24),(131,24),(139,24),(128,25),(129,25),(131,25),(139,25),(128,26),(129,26),(131,26),(139,26),(140,26),(128,27),(129,27),(131,27),(139,27),(130,28),(131,28),(139,28),(140,28),(131,29),(139,29),(130,30),(131,30),(141,30),(130,31),(131,31),(139,31),(131,32),(139,32),(140,32),(130,33),(131,33),(139,33),(130,34),(131,34),(140,34),(131,35),(141,35),(131,36),(141,36),(131,37),(139,37),(141,37),(133,39),(132,40),(133,40),(134,40),(132,41),(134,41),(132,42),(132,43),(133,43),(134,43),(133,44),(134,44),(133,45),(134,47),(137,53),(138,53),(137,54),(138,54),(137,55),(138,55),(137,56),(138,56),(137,57),(138,57);
/*!40000 ALTER TABLE `Game_Photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Photo`
--

DROP TABLE IF EXISTS `Photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Photo` (`id_city`),
  KEY `fk_Photo_1` (`id_user`),
  CONSTRAINT `fk_Photo` FOREIGN KEY (`id_city`) REFERENCES `City` (`id`),
  CONSTRAINT `fk_Photo_1` FOREIGN KEY (`id_user`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Photo`
--

LOCK TABLES `Photo` WRITE;
/*!40000 ALTER TABLE `Photo` DISABLE KEYS */;
INSERT INTO `Photo` VALUES (17,'photos/Nancy/phpuFYp2R_0dd974055d_jpg.jpg','6.183238554000809','48.69362637491766',11,'2015-02-13 17:33:20',1),(18,'photos/Nancy/phpTzDDA1_56263459_jpg.jpg','6.181763339042618','48.69576153644195',11,'2015-02-13 17:35:14',1),(19,'photos/Nancy/phpJhsSjY_77412183_jpg.jpg','6.176865625381424','48.694174076173',11,'2015-02-13 17:36:39',1),(20,'photos/Nancy/phpUz3j18_Gloriette_Musicale_Pepiniere_Nancy_jpg.jpg','6.182487535476639','48.69698104190352',11,'2015-02-13 17:38:11',1),(21,'photos/Nancy/phpdPtP1W_maxikebab_jpg.jpg','6.166168975830033','48.68302358511893',11,'2015-02-13 17:40:40',1),(22,'photos/Nancy/phpJhzXuf_DSCN4341_JPG.JPG','6.161147880554154','48.6828464988683',11,'2015-02-13 17:41:52',1),(23,'photos/Terre_du_Milieu/phpp5Dl4b_1423866087_maxresdefault_1_jpg.jpg','-27.509765625','53.38332836757158',13,'2015-02-13 23:24:56',89),(24,'photos/Terre_du_Milieu/phpdyO0Ct_1423866125_mt_png.png','-46.49414062','49.439556958940855',13,'2015-02-13 23:26:30',89),(25,'photos/Terre_du_Milieu/phpNIYYtt_1423866107_mt_doom_jpg.jpg','-32.255859375','52.42922227795516',13,'2015-02-13 23:27:04',89),(26,'photos/Terre_du_Milieu/php5he8Q3_1423866130_vlcsnap_2012_09_16_23h46m09s186_png.png','-76.904296875','57.0407298383609',13,'2015-02-13 23:27:46',89),(27,'photos/Terre_du_Milieu/phpOb1ifF_1423866500_hobbiton_jpg.jpg','-115.6640625','74.913708156753',13,'2015-02-13 23:28:50',89),(28,'photos/Terre_du_Milieu/phpbN0j95_1423866110_rivendell_the_hobbit_png.png','-74.8828125','75.51915125947707',13,'2015-02-13 23:40:34',89),(29,'photos/Terre_du_Milieu/phpIk9uFl_1423866091_edoras_jpg.jpg','-73.125','56.26776108757582',13,'2015-02-13 23:42:44',89),(30,'photos/Terre_du_Milieu/php5myAvB_1423867478_weathertop_jpg.jpg','-95.009765625','75.49715731893085',13,'2015-02-13 23:45:19',89),(31,'photos/Terre_du_Milieu/phpVQdqRT_1423867691_thehobbit_gollums_cave_2_jpg.jpg','-69.345703125','76.39331166244496',13,'2015-02-13 23:48:38',89),(32,'photos/Terre_du_Milieu/phpMHsoBr_1423867787_lotr_fotr_striders_entrance_png.png','-101.42578125','75.07300992968264',13,'2015-02-13 23:50:37',89),(33,'photos/Terre_du_Milieu/phptO3D3N_ss__2015_02_13_at_11_51_46__jpg.jpg','-38.935546875','57.18390185831185',13,'2015-02-13 23:53:14',89),(34,'photos/Terre_du_Milieu/phpO8t0ik_Ent_army_png.png','-73.388671875','64.2445947679819',13,'2015-02-13 23:55:46',89),(35,'photos/Terre_du_Milieu/phpb5M9me_ss__2015_02_13_at_11_58_05__jpg.jpg','-46.0546875','57.37393841871411',13,'2015-02-13 23:59:36',89),(36,'photos/Terre_du_Milieu/phpjrYGs3_Qrtdfmv9_jpg.jpg','-83.232421875','61.64816245852389',13,'2015-02-14 00:01:30',89),(37,'photos/Terre_du_Milieu/phpKsAnvj_ss__2015_02_14_at_12_01_54__jpg.jpg','-55.37109375','69.53451763078358',13,'2015-02-14 00:03:21',89),(39,'photos/Westeros/phpNPekA3_The_Wall_jpg.jpg','-38.671875','66.37275500247459',14,'2015-02-14 00:30:44',89),(40,'photos/Westeros/phpPdHfgV_Winterfell_jpg.jpg','-75.05859375','11.35079672238376',14,'2015-02-14 00:30:09',89),(41,'photos/Westeros/phpVxccEh_ss__2015_02_14_at_12_10_16__jpg.jpg','-32.87109375','-86.3239231930507',14,'2015-02-14 00:29:11',89),(42,'photos/Westeros/phpNtHXNp_ss__2015_02_14_at_12_13_06__jpg.jpg','-71.3671875','-42.94033923363176',14,'2015-02-14 00:37:03',89),(43,'photos/Westeros/phppcMSFD_tumblr_m28pdguDFA1roz5p1o1_500_png.png','-138.8671875','-78.52557254138314',14,'2015-02-14 00:37:59',89),(44,'photos/Westeros/phpdchI9f_GoTAryaTwins_jpg.jpg','-85.4296875','-70.61261423801925',14,'2015-02-14 00:28:46',89),(45,'photos/Westeros/phpc8r6xC_Castle_Black_jpg.jpg','-37.44140625','65.73062649311034',14,'2015-02-14 00:32:42',89),(46,'photos/Westeros/phpEkzk2F_Eyrie_S4_jpg.jpg','-24.9609375','-74.40216259842437',14,'2015-02-14 01:55:45',89),(47,'photos/Westeros/phpxcXenF_Dragonstone__day__jpg.jpg','15.46875','-83.97925949886204',14,'2015-02-14 00:44:20',89),(48,'photos/Paris/phpTou4TL_708114_tour_eiffel_paris_jpg.jpg','2.2944045066833496','48.858339353723544',12,'2015-02-14 02:03:10',89),(49,'photos/Paris/phpGrgZdP_bigstock_arc_of_triumph_3173116_jpg.jpg','2.2950348258018494','48.87379452227573',12,'2015-02-14 02:04:26',89),(50,'photos/Paris/phpmAYA0B_parisnotredamepano2_jpg.jpg','2.3499611020088196','48.85303332047154',12,'2015-02-14 02:06:00',89),(51,'photos/Paris/phph9zuI6_paris_basilique_sacre_coeur_montmartre_jpg.jpg','2.3430396616458893','48.88671133600892',12,'2015-02-14 02:08:16',89),(52,'photos/Paris/php9cwj3x_ss__2015_02_14_at_02_08_26__jpg.jpg','2.312892973423004','48.858317074545226',12,'2015-02-14 02:10:12',89),(53,'photos/New_York/phpkEz7s5_ss__2015_02_14_at_02_11_41__jpg.jpg','-74.04453903436661','40.68928300652803',15,'2015-02-14 02:12:47',89),(54,'photos/New_York/phpuEUA1B_ss__2015_02_14_at_02_12_48__jpg.jpg','-73.96532535552979','40.78276332407907',15,'2015-02-14 02:13:50',89),(55,'photos/New_York/phpcYoACM_ss__2015_02_14_at_02_13_55__jpg.jpg','-73.96324396133423','40.77943147340271',15,'2015-02-14 02:15:08',89),(56,'photos/New_York/phpsgGM8P_MOMA1_jpg.jpg','-73.97762328386307','40.761427601992416',15,'2015-02-14 02:17:22',89),(57,'photos/New_York/phpCwVqxR_ss__2015_02_14_at_02_17_34__jpg.jpg','-73.98554787039757','40.7580363968557',15,'2015-02-14 02:19:43',89),(58,'photos/Terre_du_Milieu/phprvAr1S_argona10_jpg.jpg','-55.634765625','59.0405546167585',13,'2015-02-14 02:26:50',89);
/*!40000 ALTER TABLE `Photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'Jordan','jordanbeckerfr@gmail.com','$2y$12$1RV4Y3VQg.VtEqJ.j9N4s.0OOAaTdr3oEIekCj/GjX3tpXI2ozorS',1,'2015-02-09 12:06:07'),(76,'Sample User','a@a.com','$2y$12$1RV4Y3VQg.VtEqJ.j9N4s.0OOAaTdr3oEIekCj/GjX3tpXI2ozorS',NULL,'2015-02-13 10:29:42'),(86,'Jordan',NULL,NULL,NULL,NULL),(87,'Jordan',NULL,NULL,NULL,NULL),(88,'Jordan',NULL,NULL,NULL,NULL),(89,'Jacques','aaa@aaa.com','$2y$12$VYl4.svtfisW.ODDmd81euhDZZsI4kYw6ykGJ.miNo8ny6ycTFyri',1,'2015-02-13 22:39:55'),(90,'Manon',NULL,NULL,NULL,NULL),(91,'Manon',NULL,NULL,NULL,NULL),(92,'Manon',NULL,NULL,NULL,NULL),(93,'Manon',NULL,NULL,NULL,NULL),(94,'Manon',NULL,NULL,NULL,NULL),(95,'Manon',NULL,NULL,NULL,NULL),(96,'Manon',NULL,NULL,NULL,NULL),(97,'bbb','bbb@bbb.com','$2y$12$1luFknj0sBTpGdIFYlYDPeb3c4/SSCcWlop4I6tCnHO.4OZ3Uu/fy',NULL,'2015-02-14 01:32:35'),(98,'Manon',NULL,NULL,NULL,NULL),(99,'Manon',NULL,NULL,NULL,NULL),(100,'Manon',NULL,NULL,NULL,NULL),(101,'Manon',NULL,NULL,NULL,NULL),(102,'Manon',NULL,NULL,NULL,NULL),(103,'Manon',NULL,NULL,NULL,NULL),(104,'Manon',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-14  2:39:57
