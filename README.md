# Atelier

## Installation

A la racine du projet, executer la commande suivante

	php composer.phar install


## Base de données

Dupliquer le fichier config.ini.default et le renommer en config.ini.

*NE PAS SUPPRIMER LE FICHIER CONFIG.INI.DEFAULT*

Créer la base de données puis importer le fichier sql dans le dossier sql/ à la racine

## Foundation SASS

Il est nécessaire d'avoir Bower installé.
Ensuite, dans le répertoire du projet :

    bower install


## Préprocesseurs

### CoffeeScript

A la racine du projet :

	cd public/js/
	coffee -wo ./dist .

### Compass

A la racine du projet :

	compass watch

## Résultat

Si vous allez sur *localhost/atelier*, vous devirez obtenir un design semblable

![alt text](http://puu.sh/fsRK7/56e49ed2ad.png "Rendu")
