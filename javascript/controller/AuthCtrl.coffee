app = angular.module 'app'

#Authentification de l'utilisateur
app.controller 'AuthCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', ($scope, $http, $q, $rootScope, $location, startGame) ->

    if localStorage.username != undefined
        $scope.user = {}
        $scope.user.username = localStorage.username

    #On redirige l'utilisateur vers la sélection de la ville / difficulté quand il clique sur Commencer
    $scope.loadGame = ->
        localStorage.setItem 'username', $scope.user.username
        $location.path '/choose-city'

    #On connecte l'utilisateur
    $scope.signIn = ->
        console.log($scope.user)
]