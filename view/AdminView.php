<?php

namespace view;

class AdminView extends View {

    public function __construct() {
        $this->layout = "admin.html.twig";
    }

}