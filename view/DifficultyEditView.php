<?php

namespace view;

class DifficultyEditView extends View {

    public function __construct() {
        $this->layout = "admin-difficulty-edit.html.twig";
    }

}