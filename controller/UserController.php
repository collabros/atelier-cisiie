<?php

namespace controller;

use exception\AuthException;
use \view\Inscription;
use \view\Login;
use \model\User;

class UserController extends BaseController {

    public function register($params=null) {
        if(isset($_SESSION['connecte'])) {
            $this->app->flash("erreur", "Vous êtes déjà connecté");
            $this->app->redirect($this->app->urlFor('index'));
        }



        $token = generateToken();
        $_SESSION['token']=$token;

        $view = new Inscription();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('token', $_SESSION['token']);
        $view->addVar('postURL', $this->app->urlFor('registerPost'));
        $view->addVar('title', 'PhotoLocate - Inscription');

        if(is_array($params)) {
            foreach ($params as $key => $param) {
                $view->addVar($key, $param);
            }
        }


        echo $view->render();
    }

    public function registerPost() {
        if($_SESSION['token'] != $_POST['token']) {
            $this->app->flash("error", "Clé d'identification invalide");
            $this->app->redirect($this->app->urlFor('register'));
        }

        $res = $this->app->request->post();
        $errors = array();
        $user = new User();

        UserController::sanitize($res, $user, $errors);

        if(sizeof($errors) == 0) {
            $user->created_at=date('Y-m-d H:i:s');
            $user->save();
        }
        if(sizeof($errors) > 0) {
            $pre=array('errors' => $errors, 'user' => $user);

            $this->register($pre);
            exit;
        }

        Authentication::loadProfile($user->mail);

        $this->app->flash('success', 'Inscription réussie');
        $this->app->redirect($this->app->urlFor('admin'));
    }

    public function login() {
        $view=new Login();
        $view->addVar('postURL', $this->app->urlFor('loginPost'));
        $view->addVar('title', 'PhotoLocate - Connexion');
        $view->addVar('link', links());
        $view->addVar('session', $_SESSION);

        echo $view->render();
    }

    public function loginPost() {
        $res=$this->app->request->post();
        try {
            Authentication::authenticate($res['mail'], $res['password']);
        }
        catch (AuthException $e) {
            $this->app->flash('error', "Echec de l'authentification");
            $this->app->redirect($this->app->urlFor('login'));
        }
        Authentication::loadProfile($res['mail']);

        $this->app->flash('success', 'Connexion réussie');
        $this->app->redirect($this->app->urlFor('admin'));
    }


    /**
     * @param $data Array, Data to sanitize
     * @param $user User, in which to add the verified data
     * @param $errors Array, fill up with occuring errors
     */
    public static function sanitize($data, &$user, &$errors) {
        if (array_key_exists('username', $data)) {
            UserController::sanitizeUsername($data['username'], $user, $errors);
        } else {
            $errors['username']="Le champ nom d'utilisateur ne peut pas être vide";
        }
        if (array_key_exists('mail', $data)) {
            UserController::sanitizeMail($data['mail'], $user, $errors);
        } else {
            $errors['mail']="Le champ mail ne peut pas être vide";
        }
        if (array_key_exists('password', $data) && array_key_exists('password_conf', $data)) {
            UserController::sanitizePassword($data['password'], $data['password_conf'], $user, $errors);
        } else {
            $errors['password']="Le champ mot de passe ne peut pas être vide";
        }


    }

    public static function sanitizeUsername($data, &$user, &$errors) {
        if ($data != "") {
            $user->username=$data;
        }
        else {
            $errors['username']="Le nom d'utilisateur est invalide";
        }
    }

    public static function sanitizeMail($mail, &$user, &$errors) {
        if ($mail != "") {
            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                $already = User::where("mail", "=", $mail)->first();
                if ($already == null) {
                    $user->mail=$mail;
                }
                else {
                    $errors['mail']="Un compte existe déjà avec cette adresse";
                }
            }
            else {
                $errors['mail']="L'adresse mail spécifiée n'est pas valide";
            }
        }
        else {
            $errors['mail']="Le champ mail ne peut pas être vide";
        }

    }

    public static function sanitizePassword($pass, $conf, &$user, &$errors) {
        if ($pass == "" || $conf == "") {
            $errors['password']="Le mot de passe ne peut pas être vide";
        }
        else {
            if ($pass == $conf) {
                $user->password = password_hash($pass, PASSWORD_DEFAULT, array('cost'=> 12));
            }
            else {
                $errors['password']="Les mots de passe ne correspondent pas";
            }
        }
    }

    public function deconnexion() {
        session_unset();
        session_destroy();
        session_start();
        $this->app->flash('success', 'Déconnexion réussie');
        $this->app->redirect($this->app->urlFor('play'));
    }

}